from flask import Flask
from api_src.config import AppConfig
from api_src.api_list import api_list


def create_app(app_config: AppConfig = AppConfig) -> Flask:
    app = Flask(__name__)
    app.config.from_object(app_config)

    with app.app_context():
        for available_api in api_list:
            app.register_blueprint(available_api)
        register_app_handler(app)
    return app


def register_app_handler(app: Flask):
    @app.errorhandler(400)
    def bad_request(error):
        return error, error.code
