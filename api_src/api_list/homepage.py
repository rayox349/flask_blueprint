from flask import Blueprint
from flask import current_app
from flask import redirect, url_for
from flask import abort

homepage_api = Blueprint('homepage_api', __name__)


@homepage_api.route('/', methods=['GET'])
def index():
    return f'This is homepage at {current_app.config["ENV"]}'


@homepage_api.route("/index", methods=['GET'])
def index_():
    return redirect(url_for("homepage_api.index"))


@homepage_api.route("/400", methods=['GET'])
def index_400():
    abort(400)
