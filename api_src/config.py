import logging
import os
from dotenv import dotenv_values
from pathlib import Path


class ConfigError(Exception):
    pass


class AppConfig(object):
    TZ = os.getenv('TZ', None)
    DEBUG = os.getenv('DEBUG', False)
    ENV = os.getenv('ENV', None)

    def __init__(self, env_file='local.env'):
        self.env_file = env_file
        self.environment_setter()

    def environment_setter(self):
        logging.getLogger().setLevel(logging.INFO)
        if os.path.exists('/.dockerenv'):
            logging.info("Starting in Docker")
            return

        if Path(self.env_file).is_file():
            for env_key, env_value in dotenv_values(self.env_file).items():
                if env_value.startswith('~'):
                    env_value = Path(env_value).expanduser()
                if env_value in ["True", "False"]:
                    env_value = eval(env_value)
                setattr(AppConfig, env_key, env_value)
            logging.info("Starting in Local")
            return

        error_message = 'Please check env file status. Setting is not available !'
        logging.error(f"{error_message}, Exiting...")
        raise ConfigError(error_message)
