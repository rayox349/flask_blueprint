from api_src import create_app
from api_src.config import AppConfig

app = create_app(app_config=AppConfig())

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5555)
